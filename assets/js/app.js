import { view_home } from "./home.js"
import { view_application } from "./applications.js"
import { view_category } from "./categories.js"
import { logged_in, view_login, view_register, view_profile, logout } from "./auth.js";
import { view_submission } from "./submissions.js";

// let app = {
//     render: (path) => {
//         console.log(`goto ${ path }`)
//         app.paths.forEach(p => {
//             let match = path.match(p.regex)
//             if (match.length === 3) {
//                 p.view(match[2])
//             } else {
//                 p.view()
//             }
//         })
//     },
//     // [{regex, view_func}]
//     paths: [
//         { regex: /^\/$/, view: view_home },
//         { regex: /^\/login$/, view: view_login },
//     ],
//     isValid: (path) => {
//         app.paths.forEach(p => {
//             if (path.match(p.regex)) {
//                 return true
//             }
//         })
//         return false
//     }
// }

// if ("serviceWorker" in navigator) {
//     window.addEventListener("load", function() {
//         navigator.serviceWorker
//             .register("/js/serviceWorker.js", {scope: "/"})
//             .then(res => console.log("service worker registered:", res))
//             .catch(err => console.log("service worker not registered", err))
//     })
// }

document.addEventListener('DOMContentLoaded', main, false)

// Catch clicks on the root-level element.
// document.body.addEventListener("click", function(event) {
//     let tag = event.target
//     if (tag.tagName === "A" && tag.href && event.button === 0) {
//         // It's a left click on an <a href=...>.
//         if (tag.origin === document.location.origin) {
//             // It's a same-origin navigation: a link within the site.
//
//             // Now check that the app is capable of doing a
//             // within-page update.  (You might also take .query into
//             // account.)
//             // var oldPath = document.location.pathname;
//             let newPath = tag.pathname;
//             if (app.isValid(newPath)) {
//                 // Prevent the browser from doing the navigation.
//                 e.preventDefault();
//                 // Let the app handle it.
//                 app.render(newPath);
//                 history.pushState(null, '', path);
//             }
//         }
//     }
// })

// Also transition when the user navigates back.
// window.onpopstate = function(event) {
//     if (app.isValid(document.location.pathname)) {
//         app.render(document.location.pathname)
//         event.preventDefault()
//     }
// }

export async function build_top_nav() {
  const user = await logged_in()
  // if (user) {
  //   const user_response = await fetch(`/api/users/${logged_in.user_id}`, {
  //     headers: {
  //       Authorization: `Bearer: ${jwt}`
  //     }
  //   })
  //   user = await user_response.json()
  // }

  // build top nav
  const nav_ul = document.querySelector("#top > ul")

  // empty ul contents
  while(nav_ul.firstChild){
    nav_ul.removeChild(nav_ul.firstChild);
  }

  if (user === null) {
    const login_li = document.createElement("li")
    const login_a = document.createElement("a")
    login_a.href = "#"
    login_a.innerText = "Login"
    login_a.onclick = () => view_login()
    login_li.appendChild(login_a)
    nav_ul.appendChild(login_li)

    const register_li = document.createElement("li")
    const register_a = document.createElement("a")
    register_a.href = "#"
    register_a.innerText = "Register"
    register_a.onclick = () => view_register()
    register_li.appendChild(register_a)
    nav_ul.appendChild(register_li)
  } else {
    const profile_li = document.createElement("li")
    const profile_a = document.createElement("a")
    const profile_img = document.createElement("img")
    profile_img.src = `/users/${user.id}`
    profile_img.classList.add("avatar")
    // profile_img.alt = `profile picture of ${user.name}`
    profile_a.appendChild(profile_img)
    profile_a.href = "#"
    profile_a.innerHTML += ` ${user.name}`
    profile_a.onclick = () => view_profile(user.id)
    profile_li.appendChild(profile_a)
    nav_ul.appendChild(profile_li)

    const logout_li = document.createElement("li")
    const logout_a = document.createElement("a")
    logout_a.href = "#"
    logout_a.innerText = "Logout"
    logout_a.onclick = () => logout()
    logout_li.appendChild(logout_a)
    nav_ul.appendChild(logout_li)
  }
}

async function main() {
  build_top_nav()

  // build secondary navigation
  const categories_response = await fetch("/api/categories")
  const categories = await categories_response.json()

  const categories_ul = document.getElementById("categories")

  for(const category of categories) {
    const category_li = document.createElement("li")
    category_li.id = "category_" + category.id
    // const category_a = document.createElement("a")
    // category_a.href = "#"
    // category_a.innerText = category.name
    // category_a.onclick = () => view_category(category.id)
    // category_li.appendChild(category_a)
    category_li.innerText = category.name
    category_li.appendChild(document.createElement("ul"))
    categories_ul.appendChild(category_li)
  }

  const applications_response = await fetch("/api/applications")
  const applications = await applications_response.json()
  for(const application of applications) {
    // TODO: XenGi/dotfiles#1 fix uncategorized applications
    // if (application.category_id === null) application.category_id = 0

    const category_ul = document.querySelectorAll(`li#category_${ application.category_id } > ul`)[0]
    const application_li = document.createElement("li")
    application_li.id = `application_${ application.id }`
    const application_a = document.createElement("a")
    application_a.href = "#"
    application_a.innerText = application.name
    application_a.onclick = () => view_application(application.id)
    application_li.appendChild(application_a)
    category_ul.appendChild(application_li)
  }

  // TODO: XenGi/dotfiles#1 applications without category
  // let li = document.createElement("li")
  // li.id = "category_0"
  // let a = document.createElement("a")
  // a.href = "#"
  // a.innerText = "Uncategorized"
  // a.onclick = () => view_category(0)
  // li.appendChild(a)
  // li.appendChild(document.createElement("ul"))
  // ul.appendChild(li)

  // TODO: make this better
  // "router"
  const found = window.location.pathname.match(/^\/(\w+)\/(\d+)$/)
  if (found) {
    switch (found[1]) {
      case "applications":
        view_application(found[2])
        break
      case "categories":
        view_category(found[2])
        break
      case "submissions":
        view_submission(found[2])
        break
      case "users":
        view_profile(found[2])
        break
      default:
        view_home()
    }
  } else {
    view_home()
  }
}
