import {view_profile} from "./auth.js";

export async function view_submission(id) {
  const submission_response = await fetch(`/api/submissions/${ id }`)
  const submission = await submission_response.json()

  console.log(submission)
  window.history.pushState("", "", `/submissions/${ submission.id }`)
  document.title = `dotfiles :: submissions :: ${ submission.name }`

  const user_response = await fetch(`/api/users/${ submission.user_id }`)
  const user = await user_response.json()

  console.log(user)
  const section = document.getElementById("content")

  const h1 = document.createElement("h1")
  h1.innerText = `Submission: ${ submission.name } by `
  const a = document.createElement("a")
  a.href = "#"
  a.innerText = user.name
  a.onclick = () => view_profile(user.id)
  h1.appendChild(a)

  const screenshots = document.createElement("article")
  screenshots.id = "screenshots"
  const screenshots_h2 = document.createElement("h2")
  screenshots_h2.innerText = "Screenshots:"
  screenshots.appendChild(screenshots_h2)
  const files = document.createElement("article")
  files.id = "files"
  const configs_h2 = document.createElement("h2")
  configs_h2.innerText = "Configuration files:"
  files.appendChild(configs_h2)

  // empty section contents
  while(section.firstChild){
      section.removeChild(section.firstChild);
  }
  section.appendChild(h1)
  section.appendChild(screenshots)
  section.appendChild(files)

  const attachments_response = await fetch(`/api/attachments?submission_id=${ submission.id }`)
  const attachments = await attachments_response.json()
  for(const attachment of attachments) {
    console.log(attachment)
    if (attachment.screenshot) {
      const a = document.createElement("a")
      a.href = `/media/${ submission.id }/${ attachment.id }`
      a.download = attachment.filepath.split("/").at(-1)
      const img = document.createElement("img")
      img.src = `/media/${ submission.id }/${ attachment.id }`
      // img.alt = ""
      a.appendChild(img)
      screenshots.appendChild(a)
    } else {
      const h3 = document.createElement("h3")
      const code = document.createElement("code")
      code.innerText = attachment.filepath
      h3.appendChild(code)
      const pre = document.createElement("pre")
      files.appendChild(h3)
      files.appendChild(pre)

      const configfile_response = await fetch(`/media/${ submission.id }/${ attachment.id }`)
      pre.innerText = await configfile_response.text()
    }
  }
}
