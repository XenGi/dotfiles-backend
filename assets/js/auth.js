import { view_submission } from "./submissions.js";
import { view_application } from "./applications.js";
import { build_top_nav } from "./app.js";

function message_dialog(message, buttons) {
  const dialog = document.getElementById("message")
  const dialog_text = document.querySelectorAll("#message p")
  dialog_text.item(0).innerText = message
  const dialog_footer = document.querySelectorAll("#message footer")
  for(const text of buttons) {
    const button = document.createElement("button")
    button.innerText = text
    dialog_footer.item(0).appendChild(button)
  }
  dialog.showModal()
}

export async function logged_in() {
  // TODO: show login window when needed
  const jwt = localStorage.getItem("jwt")

  if (jwt != null) {
    console.log(`Found jwt: ${jwt}`)

    const body = jwt.split(".")?.[1];
    const user = JSON.parse(atob(body))
    console.log(user)

    // check if login is exprired
    if (new Date(user.exp * 1000) > Date.now()) {
      return user
    }
  }
  return null
}

export async function view_login() {
  console.log("show login form")
  const dialog = document.getElementById("login")
  dialog.showModal()
  dialog.addEventListener("close", async () => {
    if (dialog.returnValue === "forgot") {
      const username = document.getElementById("login_username").value
      if (username !== "") {
        console.log(`Sending password reset mail to user ${ username }.`)
      } else {
        console.log("No username given.")
      }
    }

    if (dialog.returnValue === "login") {
      const username = document.getElementById("login_username").value
      const password = document.getElementById("login_password").value
      if (username !== "" && password !== "") {
        // TODO: login
        console.log(`Logging in user ${ username } with password ${ password }`)

        const login_response = await fetch("/login", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            username: username,
            password: password
          })
        })
        const response = await login_response.json()

        if ("message" in response) {
          dialog.showModal()
          const err = document.getElementById("login_error")
          err.innerText = response.message
          console.error("Login error:", response.message)
        } else if("token" in response) {
          localStorage.setItem("jwt", response.token)
          console.log(`Logged in! Got token: ${response.token}`)
          build_top_nav()
        } else {
          const err = document.getElementById("login_error")
          err.innerText = "Unexpected error"
          console.error(`Unexpected response: ${ response }`)
        }
      }
    }

    // TODO: clear input on login or cancel?
    const inputs = document.querySelectorAll("#login input")
    for(const input of inputs) {
      input.value = ""
    }
    const err = document.getElementById("login_error")
    err.innerText = ""
  });
}

export async function view_register() {
  console.log("show register form")

  const password = document.getElementById("register_password")
  const password2 = document.getElementById("register_password2")
  const onchange = () => {
    if (password.value === password2.value) {
      password2.setCustomValidity("");
    } else {
      password2.setCustomValidity("Passwords do not match");
    }
  }
  password.onchange = onchange
  password2.onchange = onchange

  const dialog = document.getElementById("register")
  dialog.showModal()
  dialog.addEventListener("close", async () => {
    if (dialog.returnValue === "register") {
      const username = document.getElementById("register_username").value
      const email = document.getElementById("register_email").value
      if (password.value !== "" && password.value !== password2.value) {
        console.log("Passwords don't match")
      } else if (username !== "" && email !== "" && password.value !== "" && password.value === password2.value) {
        console.log(`Registering user ${username} with email ${ email } password ${ password.value }`)
        // TODO: register
        const register_response = await fetch("/register", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          redirect: "follow",
          body: JSON.stringify({
            "username": username,
            "email": email,
            "password": password.value
          })
        })
        const response = await register_response.json()
        if ("message" in response) {
          dialog.showModal()
          const err = document.getElementById("register_error")
          err.innerText = response.message
          console.error("Login error:", response.message)
        } else if("token" in response) {
          console.log(`Registered! Got token: ${response.token}`)
          localStorage.setItem("jwt", response.token)
        } else {
          const err = document.getElementById("register_error")
          err.innerText = "Unexpected error"
          console.error(`Unexpected response: ${ response }`)
        }
      }
    }

    // TODO: clear input on register or cancel?
    const inputs = document.querySelectorAll("#register input")
    for(const input of inputs) {
      input.value = ""
    }
  });
}

export async function view_new_submission() {
  // TODO: build submission form
}

export async function view_profile(id) {
  const jwt = logged_in()
  if (jwt != null && jwt.user_id === id) {
    // TODO: if id is mine, add "edit profile" and "change password" buttons etc
  }

  const user_response = await fetch(`/api/users/${ id }`)
  const user = await user_response.json()

  console.log(user)
  window.history.pushState("", "", `/users/${ user.id }`)
  document.title = `dotfiles :: users :: ${ user.name }`

  console.log(user)

  const section = document.getElementById("content")
  const h1 = document.createElement("h1")
  h1.innerText = `Profile of ${ user.name }`

  // empty section contents
  while(section.firstChild){
    section.removeChild(section.firstChild);
  }
  section.appendChild(h1)

  const ul = document.createElement("ul")

  const email_li = document.createElement("li")
  email_li.innerHTML = `<b>Email:</b> ${ user.email }`
  ul.appendChild(email_li)

  const joined_li = document.createElement("li")
  joined_li.innerHTML = `<b>Joined:</b> ${ new Date(user.joined).toLocaleString() }`
  ul.appendChild(joined_li)

  const seen_li = document.createElement("li")
  seen_li.innerHTML = `<b>Last seen:</b> ${ new Date(user.last_seen).toLocaleString() }`
  ul.appendChild(seen_li)

  section.appendChild(ul)

  const h2 = document.createElement("h2")
  h2.innerText = "Submissions:"
  section.appendChild(h2)

  const table = document.createElement("table")
  const tr = document.createElement("tr")

  const name_th = document.createElement("th")
  name_th.innerText = "Name"
  tr.appendChild(name_th)

  const application_th = document.createElement("th")
  application_th.innerText = "Application"
  tr.appendChild(application_th)

  const updated_th = document.createElement("th")
  updated_th.innerText = "Updated"
  tr.appendChild(updated_th)

  const created_th = document.createElement("th")
  created_th.innerText = "Created"
  tr.appendChild(created_th)

  table.appendChild(tr)

  section.appendChild(table)

  const submissions_response = await fetch(`/api/submissions?user_id=${ user.id }`)
  const submissions = await submissions_response.json()
  for(const submission of submissions) {
    const application_response = await fetch(`/api/applications/${ submission.application_id }`)
    const application = await application_response.json()

    const tr = document.createElement("tr")

    const name_td = document.createElement("td")
    const name_a = document.createElement("a")
    name_a.href = "#"
    name_a.innerText = submission.name
    name_a.onclick = () => view_submission(submission.id)
    name_td.appendChild(name_a)
    tr.appendChild(name_td)

    const application_td = document.createElement("td")
    const application_a = document.createElement("a")
    application_a.href = "#"
    application_a.innerText = application.name
    application_a.onclick = () => view_application(application.id)
    application_td.appendChild(application_a)
    tr.appendChild(application_td)

    const updated_td = document.createElement("td")
    updated_td.innerText = new Date(submission.updated).toLocaleString()
    tr.appendChild(updated_td)

    const created_td = document.createElement("td")
    created_td.innerText = new Date(submission.created).toLocaleString()
    tr.appendChild(created_td)

    table.appendChild(tr)
  }
}

export async function logout() {
  // TODO: do the logout dance
  console.log("Deleting jwt if found")
  localStorage.removeItem("jwt")
  build_top_nav()
}
