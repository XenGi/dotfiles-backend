export async function view_home() {
    window.history.pushState("", "", "/")
    document.title = "dotfiles :: home"

    const section = document.getElementById("content")

    const h1 = document.createElement("h1")
    h1.innerText = "dotfiles"
    const h2 = document.createElement("h2")
    h2.innerText = "Share your config"
    const p1 = document.createElement("p")
    p1.innerText = "Awesome text that will make you want to use this site immediately!"
    const p2 = document.createElement("p")
    p2.innerText = "Another paragraph, maybe with some statistics about how many apps or config files got uploaded so far?"

    // empty section contents
    while(section.firstChild){
        section.removeChild(section.firstChild);
    }
    section.appendChild(h1)
    section.appendChild(h2)
    section.appendChild(p1)
    section.appendChild(p2)
}