import { view_profile } from "./auth.js";
import { view_submission } from "./submissions.js";

export async function view_application(id) {
  const response = await fetch(`/api/applications/${id}`)
  const application = await response.json()
  console.log(application)
  window.history.pushState("", "", `/applications/${ application.id }`)
  document.title = `dotfiles :: applications :: ${ application.name }`

  const section = document.getElementById("content")

  const h1 = document.createElement("h1")
  h1.innerText = `Listing submissions for ${ application.name }`

  const table = document.createElement("table")
  const thead_tr = document.createElement("tr")

  const name_th = document.createElement("th")
  name_th.innerText = "Name"
  thead_tr.appendChild(name_th)

  const user_th = document.createElement("th")
  user_th.innerText = "User"
  thead_tr.appendChild(user_th)

  const updated_th = document.createElement("th")
  updated_th.innerText = "Updated"
  thead_tr.appendChild(updated_th)

  const created_th = document.createElement("th")
  created_th.innerText = "Created"

  thead_tr.appendChild(created_th)
  table.appendChild(thead_tr)

  // empty section contents
  while(section.firstChild){
    section.removeChild(section.firstChild);
  }
  section.appendChild(h1)
  section.appendChild(table)

  const submission_response = await fetch(`/api/submissions?application_id=${ application.id }`)
  const submissions = await submission_response.json()
  for(const submission of submissions) {
    const response = await fetch(`/api/users/${ submission.user_id }`)
    const user = await response.json()
    console.log(submission)
    console.log(user)

    const submission_tr = document.createElement("tr")

    const name_td = document.createElement("td")
    const name_a = document.createElement("a")
    name_a.href = "#"
    name_a.innerText = submission.name
    name_a.onclick = () => view_submission(submission.id)
    name_td.appendChild(name_a)
    submission_tr.appendChild(name_td)

    const user_td = document.createElement("td")
    const user_a = document.createElement("a")
    user_a.href = "#"
    user_a.innerText = user.name
    user_a.onclick = () => view_profile(user.id)
    user_td.appendChild(user_a)
    submission_tr.appendChild(user_td)

    const updated_td = document.createElement("td")
    updated_td.innerText = new Date(submission.updated).toLocaleString()
    submission_tr.appendChild(updated_td)

    const created_td = document.createElement("td")
    created_td.innerText = new Date(submission.created).toLocaleString()
    submission_tr.appendChild(created_td)

    table.appendChild(submission_tr)
  }
}
