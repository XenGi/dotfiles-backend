INSERT INTO users (name, email, password_hash, joined, last_seen, is_admin) VALUES
('Armin', 'admin@domain.com', 'qwerty123',      '1999-09-09T09:59:59Z', '1985-10-05T11:11:11Z', 1),
('Horst', 'horst@domain.com', 'supersecure123', '1970-01-01T13:37:00Z', '2000-02-20T20:02:00Z', 0);

INSERT INTO categories (name, created) VALUES
('Text editors', '1999-09-09T09:59:59Z'),
('Shells',       '1970-01-01T13:37:00Z');

INSERT INTO applications (name, category_id, updated, created) VALUES
('vim',   (SELECT id FROM categories WHERE name='Text editors'), '1985-10-05T11:11:11Z', '2000-02-20T20:02:00Z'),
('emacs', (SELECT id FROM categories WHERE name='Text editors'), '1970-01-01T13:37:00Z', '1999-09-09T09:59:59Z'),
('bash',  (SELECT id FROM categories WHERE name='Shells'),       '1985-10-05T11:11:11Z', '2000-02-20T20:02:00Z'),
('fish',  (SELECT id FROM categories WHERE name='Shells'),       '1970-01-01T13:37:00Z', '1999-09-09T09:59:59Z'),
('zsh',   (SELECT id FROM categories WHERE name='Shells'),       '1970-01-01T13:37:00Z', '1999-09-09T09:59:59Z');

INSERT INTO submissions (name, user_id, application_id, updated, created) VALUES
('My cool vim config', (SELECT id FROM users WHERE name='Armin'), (SELECT id FROM applications WHERE name='vim'),   '1999-09-09T09:59:59Z', '1985-10-05T11:11:11Z'),
('My favorite shell',  (SELECT id FROM users WHERE name='Armin'), (SELECT id FROM applications WHERE name='bash'),  '1999-09-09T09:59:59Z', '1985-10-05T11:11:11Z'),
('emacs is the best!', (SELECT id FROM users WHERE name='Horst'), (SELECT id FROM applications WHERE name='emacs'), '1970-01-01T13:37:00Z', '2000-02-20T20:02:00Z');

INSERT INTO attachments (filepath, screenshot, submission_id, updated, created) VALUES
('~/.vimrc',                   0, 1, '2022-09-16T13:22:14Z', '2022-09-16T13:22:14Z'),
('screenshot.png',             1, 1, '2022-09-16T13:22:14Z', '2022-09-16T13:22:14Z'),
('~/.config/emacs/emacs.conf', 0, 2, '2022-09-16T13:25:34Z', '2022-09-16T13:25:34Z'),
('picture.jpg',                1, 2, '2022-09-16T13:25:34Z', '2022-09-16T13:25:34Z');
