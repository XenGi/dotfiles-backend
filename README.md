[![pipeline status](https://gitlab.com/XenGi/dotfiles/badges/main/pipeline.svg)](https://gitlab.com/XenGi/dotfiles/-/commits/main)
[![coverage report](https://gitlab.com/XenGi/dotfiles/badges/main/coverage.svg)](https://gitlab.com/XenGi/dotfiles/-/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/XenGi/dotfiles)](https://goreportcard.com/report/gitlab.com/XenGi/dotfiles)
[![latest release](https://gitlab.com/XenGi/dotfiles/-/badges/release.svg)](https://gitlab.com/XenGi/dotfiles/-/releases)
[![license](https://img.shields.io/gitlab/license/xengi/dotfiles)](https://choosealicense.com/licenses/mit/)
[![chat](https://img.shields.io/matrix/dotfiles?server_fqdn=catgirl.butt)](https://matrix.to/#/#dotfiles:catgirl.butt?via=catgirl.butt)
[![website](https://img.shields.io/website?down_color=red&down_message=offline&up_color=lime&up_message=online&url=https%3A%2F%2Fdotfiles.net)](https://dotfiles.net)
[![maintained](https://img.shields.io/maintenance/yes/2022)]()

dotfiles
========

A platform to share custom configuration files.

The backend is written in [Go][go] with the help of the [Echo framework][echo].

The frontend is written in pure [Javascript/ES6][js], [CSS3][css] and [HTML5][html] with zero dependencies, other than a modern browser.

*Disclaimer: I don't care if it doesn't work in Edge or on Windows, but don't hesitate to file an issue if it doesn't run in your obscure browser or on your weird OS. ;)*


Deployment
==========

dotfiles is packaged as a container to be easily deployable in any environment.

Here's an example using podman:

```shell
podman volume create dotfiles_db
podman run -d -p 8443 -v dotfiles_db:/data ghcr.io/xengi/dotfiles/dotfiles:latest
```

The production instance is running in kubernetes like this:

```yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: dotfiles
---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc
  namespace: dotfiles
spec:
  storageClassName: cephfs
  accessModes:
    - ReadWriteMany
  resources:
    requests:
      storage: 1Gi
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: dotfiles
  namespace: dotfiles
spec:
  selector:
    matchLabels:
      app: web
  replicas: 3
  strategy:
    type: RollingUpdate
  template:
    metadata:
      namespace: dotfiles
      labels:
        app: web
    spec:
      volumes:
        - name: db
          persistentVolumeClaim:
            claimName: pvc
      containers:
        - name: dotfiles
          image: "ghcr.io/xengi/dotfiles/dotfiles:latest"
          imagePullPolicy: Always
          ports:
            - name: https
              containerPort: 8443
              protocol: TCP
          volumeMounts:
            - name: db
              mountPath: /data
          securityContext:
            runAsNonRoot: true
            runAsUser: 1000
      securityContext:
        runAsNonRoot: true
```

Yes I know you can't access a single sqlite db from 3 pods at the same time. dotfiles is not finished yet, so leave me alone! :P

---

Made with ❤️ and ✨.


[go]: https://go.dev/
[echo]: https://echo.labstack.com/
[js]: https://developer.mozilla.org/docs/Web/JavaScript
[css]: https://developer.mozilla.org/docs/Web/CSS
[html]: https://developer.mozilla.org/docs/Web/HTML
