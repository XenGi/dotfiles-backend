FROM docker.io/golang:latest as builder

WORKDIR /go/src/gitlab.com/xengi/dotfiles

# cache dependencies?
COPY go.mod go.sum ./
RUN go mod download \
 && go mod verify

COPY . .

#RUN go test ./...
#RUN go build -v -o /go/bin/dotfiles ./...
RUN go build -v -o /go/bin/dotfiles

FROM gcr.io/distroless/base-debian11

WORKDIR /
COPY --from=builder /go/bin/dotfiles /bin/

VOLUME /data

EXPOSE 8080
EXPOSE 8443
USER nonroot:nonroot

ENTRYPOINT ["/bin/dotfiles"]
