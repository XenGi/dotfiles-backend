package handlers

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"gitlab.com/XenGi/dotfiles/models"
	"net/http"
	"strconv"
)

// GET /api/attachments
func GetAttachments(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		submission_id, err := strconv.Atoi(c.QueryParam("submission_id"))
		if err != nil {
			submission_id = -1
		}
		attachments, err := models.GetAttachments(db, submission_id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, attachments)
	}
}

// GET /api/attachments/:id
func GetAttachment(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var attachment *models.Attachment
		attachment, err = models.GetAttachment(db, id)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound)
			}
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"id":            attachment.ID,
			"filepath":      attachment.Filepath,
			"screenshot":    attachment.Screenshot,
			"submission_id": attachment.SubmissionID,
			"updated":       attachment.Updated,
			"created":       attachment.Created,
		})
	}
}

// PUT /api/attachments
func PutAttachment(db *sql.DB) echo.HandlerFunc {
	// TODO: require login
	return func(c echo.Context) error {
		var attachment models.Attachment
		err := c.Bind(&attachment)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var id int64
		id, err = models.PutAttachment(db, attachment.Filepath, attachment.Screenshot, attachment.SubmissionID)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusCreated, echo.Map{
			"created": id,
		})
	}
}

// POST /api/attachments/:id
func UpdateAttachment(db *sql.DB) echo.HandlerFunc {
	// TODO: require login; owner or admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var attachment models.Attachment
		err = c.Bind(&attachment)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.UpdateAttachment(db, id, attachment.Filepath, attachment.Screenshot)
		if err == nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"updated": id,
		})
	}
}

// DELETE /api/attachments/:id
func DeleteAttachment(db *sql.DB) echo.HandlerFunc {
	// TODO: require login; owner or admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.DeleteAttachment(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.NoContent(http.StatusNoContent)
	}
}
