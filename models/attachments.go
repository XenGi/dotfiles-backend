package models

import (
	"database/sql"
	"github.com/labstack/gommon/log"
	"time"
)

type Attachment struct {
	ID           int       `json:"id"`
	Filepath     string    `json:"filepath"`
	Screenshot   bool      `json:"screenshot"`
	SubmissionID int       `json:"submission_id"`
	Updated      time.Time `json:"updated"`
	Created      time.Time `json:"created"`
}

func GetAttachments(db *sql.DB, submission_id int) ([]Attachment, error) {
	var rows *sql.Rows
	var err error
	if submission_id > 0 {
		rows, err = db.Query("SELECT id, filepath, screenshot, submission_id, updated, created FROM attachments WHERE submission_id=?", submission_id)
	} else {
		rows, err = db.Query("SELECT id, filepath, screenshot, submission_id, updated, created FROM attachments")
	}
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	defer rows.Close()

	attachments := []Attachment{}
	for rows.Next() {
		attachment := Attachment{}
		var updated string
		var created string
		err = rows.Scan(&attachment.ID, &attachment.Filepath, &attachment.Screenshot, &attachment.SubmissionID, &updated, &created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		attachment.Updated, err = time.Parse(time.RFC3339, updated)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		attachment.Created, err = time.Parse(time.RFC3339, created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		attachments = append(attachments, attachment)
	}
	return attachments, nil
}

func GetAttachment(db *sql.DB, id int) (*Attachment, error) {
	stmt, err := db.Prepare("SELECT id, filepath, screenshot, submission_id, updated, created FROM attachments WHERE id = ?")
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}

	attachment := &Attachment{}
	var updated string
	var created string
	err = stmt.QueryRow(id).Scan(&attachment.ID, &attachment.Filepath, &attachment.Screenshot, &attachment.SubmissionID, &updated, &created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	attachment.Updated, err = time.Parse(time.RFC3339, updated)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	attachment.Created, err = time.Parse(time.RFC3339, created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}

	return attachment, nil
}

func PutAttachment(db *sql.DB, filepath string, screenshot bool, submission_id int) (int64, error) {
	// TODO: handle file upload
	stmt, err := db.Prepare("INSERT INTO attachments(filepath, screenshot, submission_id) VALUES(?, ?, ?)")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(filepath, screenshot, submission_id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func UpdateAttachment(db *sql.DB, id int, filepath string, screenshot bool) (int64, error) {
	// TODO: handle file upload
	stmt, err := db.Prepare("UPDATE attachments SET filepath = ?, screenshot = ?, updated = (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')) WHERE id=?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(filepath, screenshot, id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func DeleteAttachment(db *sql.DB, id int) (int64, error) {
	// TODO: delete file
	stmt, err := db.Prepare("DELETE FROM attachments WHERE id = ?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.RowsAffected()
}
