package models

import (
	"database/sql"
	"github.com/labstack/gommon/log"
	"time"
)

type Category struct {
	ID      int       `json:"id"`
	Name    string    `json:"name"`
	Created time.Time `json:"created"`
}

func GetCategories(db *sql.DB) ([]Category, error) {
	rows, err := db.Query("SELECT id, name, created FROM categories")
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	defer rows.Close()

	categories := []Category{}
	for rows.Next() {
		category := Category{}
		var created string
		err = rows.Scan(&category.ID, &category.Name, &created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		category.Created, err = time.Parse(time.RFC3339, created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		categories = append(categories, category)
	}
	return categories, nil
}

func GetCategory(db *sql.DB, id int) (*Category, error) {
	stmt, err := db.Prepare("SELECT id, name, created FROM categories WHERE id = ?")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	category := &Category{}
	var created string
	err = stmt.QueryRow(id).Scan(&category.ID, &category.Name, &created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	category.Created, err = time.Parse(time.RFC3339, created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}

	return category, nil
}

func PutCategory(db *sql.DB, name string) (int64, error) {
	stmt, err := db.Prepare("INSERT INTO categories(name) VALUES(?)")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func UpdateCategory(db *sql.DB, id int, name string) (int64, error) {
	stmt, err := db.Prepare("UPDATE categories SET name = ? WHERE id=?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name, id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func DeleteCategory(db *sql.DB, id int) (int64, error) {
	stmt, err := db.Prepare("DELETE FROM categories WHERE id = ?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.RowsAffected()
}
