Notes
=====

- [how to favicon](https://evilmartians.com/chronicles/how-to-favicon-in-2021-six-files-that-fit-most-needs)
- [adaptive favicon](https://web.dev/building-an-adaptive-favicon/)
- [PWA](https://github.com/ibrahima92/pwa-with-vanilla-js)
- [manifest](https://web.dev/add-manifest/)
- [service worker](https://developer.chrome.com/docs/workbox/service-worker-overview/)